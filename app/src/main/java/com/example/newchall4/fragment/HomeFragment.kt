package com.example.newchall4.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newchall4.R
import com.example.newchall4.Adapter.MenuAdapter
import com.example.newchall4.databinding.FragmentHomeBinding
import com.example.newchall4.item.MenuItem
import com.example.newchall4.ActivityLoginAccount
import com.example.newchall4.MainActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.FirebaseApp

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: MenuAdapter
    private lateinit var sharedPrefs: SharedPreferences
    private lateinit var auth: FirebaseAuth
    private val PREF_NAME = "MyPrefs"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initialize FirebaseApp
        FirebaseApp.initializeApp(requireContext())

        // Initialize FirebaseAuth
        auth = FirebaseAuth.getInstance()

        // Check if the user is already authenticated
        val currentUser: FirebaseUser? = auth.currentUser
        if (currentUser != null) {
        }
    }
        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {
            Log.d("HomeFragment", "onCreateView called")

            // Inflate the layout for this fragment
            binding = FragmentHomeBinding.inflate(inflater, container, false)
            setupMenu()

            sharedPrefs = requireActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        setupMenu()

        sharedPrefs = requireActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        val layoutManagerGrid = GridLayoutManager(requireContext(), 2)
        val layoutManagerLinear = LinearLayoutManager(requireContext())

        //Inisialisasi layout manager saat membuka aplikasi
        val savedLayout = sharedPrefs.getString("layout", "grid") ?: "grid"

        var currentLayoutManager = if (savedLayout == "grid") {
            binding.btnList.setImageResource(R.drawable.baseline_grid_view_24)
            layoutManagerGrid
        } else {
            binding.btnList.setImageResource(R.drawable.list)
            layoutManagerLinear
        }

        binding.rvMenuMakanan.layoutManager = currentLayoutManager

        binding.btnList.setOnClickListener {
            currentLayoutManager = if (currentLayoutManager == layoutManagerGrid) {
                binding.btnList.setImageResource(R.drawable.list)
                sharedPrefs.edit().putString("layout", "linear").apply()
                layoutManagerLinear
            } else {
                binding.btnList.setImageResource(R.drawable.baseline_grid_view_24)
                sharedPrefs.edit().putString("layout", "grid").apply()
                layoutManagerGrid
            }
            binding.rvMenuMakanan.layoutManager = currentLayoutManager
        }

        return binding.root
    }

    private fun setupMenu() {
        val menuItems = getMenuItems()
        adapter = MenuAdapter(menuItems) { selectedItem ->
            val bundle = Bundle().apply {
                putString("name", selectedItem.name)
                putInt("price", selectedItem.price)
                putString("description", selectedItem.description)
                putInt("imageRes", selectedItem.imageRes)
                putString("restaurantAddress", selectedItem.restaurantAddress)
                putString("googleMapsUrl", selectedItem.googleMapsUrl)
            }
            val navController =
                Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
            navController.navigate(R.id.detailActivity, bundle)
        }
        binding.rvMenuMakanan.adapter = adapter
    }

    private fun getMenuItems(): List<MenuItem> {
        val menuItems = mutableListOf<MenuItem>()
        menuItems.add(
            MenuItem(
                "Dimsum",
                20000,
                "Dimsum makanan ringan dengan proses pengolahan dikukus",
                R.drawable.dimsum,
                "Bandung",
                "https://maps.app.goo.gl/XNVQGK4B11qVT4i16"
            )
        )

        menuItems.add(
            MenuItem(
                "Kentang",
                20000,
                "kentang goreng ala Perancis",
                R.drawable.kentang,
                "Jakarta Barat",
                "https://maps.app.goo.gl/teZFogybvoLmWnDn9"
            )
        )

        menuItems.add(
            MenuItem(
                "Burger",
                15000,
                "Burger, dengan isian sayur dan daging Australia",
                R.drawable.burger,
                "Bekasi",
                "https://maps.app.goo.gl/AZgDLnyGxbiceMEX8"
            )
        )

        menuItems.add(
            MenuItem(
                "Sushi",
                35000,
                "Japanese Food yang umami",
                R.drawable.sushi,
                "Tanjung Barat",
                "https://maps.app.goo.gl/rjefyFpZtUJdTSeq9"
            )
        )

        menuItems.add(
            MenuItem(
                "Milkshake",
                10000,
                "Minuman manis yang menggugah selera",
                R.drawable.milkshake,
                "Banten",
                "https://maps.app.goo.gl/5FmQRkmwwL4Egfyh7"
            )
        )

        menuItems.add(
            MenuItem(
                "Chicken",
                25000,
                "Ayam dengan baluran Saus Spesial",
                R.drawable.ayamgoreng,
                "Garut",
                "https://maps.app.goo.gl/cMSAqxLjtVUTNP2Z9"
            )
        )

        menuItems.add(
            MenuItem(
                "Bakso",
                15000,
                "Olahan daging yang dibentuk Bakso dicampur dengan Mie",
                R.drawable.bakso,
                "Bandung",
                "https://maps.app.goo.gl/uVdTi9GgD4K7BhyG7"
            )
        )

        menuItems.add(
            MenuItem(
                "Nasi Goreng",
                20000,
                "Nasi Goreng Spesial dengan Telur Mata Sapi",
                R.drawable.nasigoreng,
                "Semarang",
                "https://maps.app.goo.gl/Cgasu9T1Jruju6KH7"
            )
        )

        menuItems.add(
            MenuItem(
                "Sate Ayan",
                25000,
                "Potongan Daging ayam yang dibuat menjadi Sate",
                R.drawable.sateayam,
                "Purwakarta",
                "https://maps.app.goo.gl/GC5eGo7AYzKszcDy7"
            )
        )

        menuItems.add(
            MenuItem(
                "Sate Maranggi",
                30000,
                "Sate dengan daging sapi khas Purwakarta",
                R.drawable.satekambing,
                "Purwakarta",
                "https://maps.app.goo.gl/GC5eGo7AYzKszcDy7"
            )
        )

        return menuItems
    }
}