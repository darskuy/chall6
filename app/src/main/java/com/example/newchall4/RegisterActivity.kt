package com.example.newchall4

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.newchall4.databinding.ActivityRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().getReference("users")

        binding.registerButton1.setOnClickListener {
            val username = binding.etUsernameValue1.text.toString().trim()
            val email = binding.etEmailValue1.text.toString().trim()
            val phone = binding.etMobileValue1.text.toString().trim()
            val password = binding.etPasswordValue1.text.toString()

            // Validasi input
            if (username.isEmpty() || email.isEmpty() || phone.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Harap mengisi kolom Username dan Password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            // Registrasi pengguna menggunakan Firebase Authentication
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // Registrasi berhasil, simpan data pengguna ke Firebase Realtime Database
                        val userId = auth.currentUser?.uid
                        if (userId != null) {
                            val user = User(userId, username, email, phone)
                            database.child(userId).setValue(user)
                        }

                        Log.d("RegisterActivity", "User registration successful")
                        Toast.makeText(applicationContext, "Registrasi berhasil", Toast.LENGTH_SHORT).show()

                        // arahkan ke LoginActivity
                        startActivity(Intent(this@RegisterActivity, ActivityLoginAccount::class.java))
                        finish()
                    } else {
                        // Registrasi gagal, log pesan kesalahan
                        Log.e("RegisterActivity", "Registrasi gagal: ${task.exception?.message}")
                        Toast.makeText(applicationContext, "Registrasi gagal: ${task.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }
        }

        binding.registerText1.setOnClickListener {
            startActivity(Intent(this@RegisterActivity, ActivityLoginAccount::class.java))
            finish()
        }
    }
}
