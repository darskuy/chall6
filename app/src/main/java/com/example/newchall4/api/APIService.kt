package com.example.newchall4.api

import com.example.newchall4.menu.MenuCategory
import com.example.newchall4.menu.MenuList
import com.example.newchall4.menu.Order
import com.example.newchall4.menu.OrderRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.Call

interface APIService {
    @GET("menulist")
    suspend fun getMenuItems(): MenuList

    @GET("menu-category")
    suspend fun getMenuCategory(): MenuCategory

    @POST("order-menu")
    suspend fun orderMenu(
        @Body orderRequest: OrderRequest,
    ): Order
}