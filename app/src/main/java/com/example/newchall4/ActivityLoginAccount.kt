package com.example.newchall4

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.newchall4.databinding.ActivityLoginAccountBinding
import com.example.newchall4.fragment.HomeFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

class ActivityLoginAccount : AppCompatActivity() {
    private lateinit var binding: ActivityLoginAccountBinding
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference
    private lateinit var authStateListener: FirebaseAuth.AuthStateListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginAccountBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().getReference("users")

        // Set up the authentication state listener
        authStateListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user: FirebaseUser? = firebaseAuth.currentUser
            if (user != null) {
                // User is signed in, navigate to HomeFragment
                navigateToHomeFragment()
            } else {
                // User is signed out, do nothing or handle as needed
            }
        }

        binding.loginButton.setOnClickListener {
            val username = binding.usernameEditText.text.toString().trim()
            val password = binding.passwordEditText.text.toString()

            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Isi username dan password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            checkUsernameInDatabase(username, password)
        }

        binding.loginText1.setOnClickListener {
            navigateToRegister()
        }
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(authStateListener)
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(authStateListener)
    }

    private fun navigateToHomeFragment() {
        val homeFragment = HomeFragment()  // Replace HomeFragment with the actual class name of your HomeFragment
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(android.R.id.content, homeFragment)  // Replace android.R.id.content with the ID of the container where you want to display your fragments
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun checkUsernameInDatabase(username: String, password: String) {
    }

    private fun navigateToRegister() {
        startActivity(Intent(this@ActivityLoginAccount, RegisterActivity::class.java))
        finish()
    }
}
